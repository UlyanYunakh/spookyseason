// Ulyan Yunakh.


#include "CharacterTargetingComponent.h"
#include "SpookySeasonCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/TimelineComponent.h"
#include <SpookySeason/Game/SpookySeasonPlayerController.h>

UCharacterTargetingComponent::UCharacterTargetingComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UCharacterTargetingComponent::BeginPlay()
{
	Super::BeginPlay();

	if (ASpookySeasonCharacter* Character = Cast<ASpookySeasonCharacter>(GetOwner()))
	{
		Controller = Cast<ASpookySeasonPlayerController>(Character->GetController());
	}

	if (!Controller)
	{
		DestroyComponent();
	}

	GetWorld()->GetTimerManager().SetTimer(TargetingTimer, this, &UCharacterTargetingComponent::TargetingTimerTick, 0.001f, true);
}

void UCharacterTargetingComponent::TargetingTimerTick()
{
	if (IsTargetingActive)
	{
		if (Target != Controller->TargetInfo.Origin)
		{
			TotatTime = 0;
			Target = Controller->TargetInfo.Origin;
		}

		float elapsed = GetWorld()->GetTimerManager().GetTimerElapsed(TargetingTimer);
		TotatTime = TotatTime > FloatCurveLenght ? elapsed : TotatTime + elapsed;

		auto FullRotator = UKismetMathLibrary::FindLookAtRotation(GetOwner()->GetActorLocation(), Target);

		float alpha = FloatCurve->GetFloatValue(TotatTime);
		FRotator NewRotator = FMath::Lerp(GetOwner()->GetActorRotation(), FRotator(0, FullRotator.Yaw, 0), alpha);
		
		GetOwner()->SetActorRotation(NewRotator);
	}
	else
	{
		TotatTime = 0;
	}
}

