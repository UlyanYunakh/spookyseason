// Ulyan Yunakh.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/TimelineComponent.h"
#include "CharacterTargetingComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SPOOKYSEASON_API UCharacterTargetingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UCharacterTargetingComponent();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
		void TargetingTimerTick();

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Targeting)
	bool IsTargetingActive = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Targeting)
	UCurveFloat* FloatCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Targeting)
	float FloatCurveLenght;

private:
	UPROPERTY()
	class ASpookySeasonPlayerController* Controller;

	UPROPERTY()
	FTimerHandle TargetingTimer;

	UPROPERTY()
	float TotatTime;

	UPROPERTY()
	FVector Target;
};
