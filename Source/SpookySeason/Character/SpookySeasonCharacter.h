// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include <SpookySeason/FunctionLibrary/SpookySeasonTypes.h>
#include "SpookySeasonCharacter.generated.h"

UCLASS(Blueprintable)
class ASpookySeasonCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ASpookySeasonCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UFUNCTION()
	void UpdateMovementState(EMovementState NewState);

public:
	UPROPERTY(EditDefaultsOnly, Category = Movement)
	EMovementState MovementState = EMovementState::Walt;

	UPROPERTY(EditDefaultsOnly, Category = Movement)
	FMovementSpeed MovementSpeedInfo;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Targeting, meta = (AllowPrivateAccess = "true"))
	class UCharacterTargetingComponent* TargetingComponent;
};

