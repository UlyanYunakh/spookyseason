// Copyright Epic Games, Inc. All Rights Reserved.

#include "SpookySeason.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SpookySeason, "SpookySeason" );

DEFINE_LOG_CATEGORY(LogSpookySeason)
 