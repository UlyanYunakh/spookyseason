// Ulyan Yunakh.


#include "Gate.h"
#include "Kismet/KismetMathLibrary.h"
#include "Camera/CameraActor.h"

AGate::AGate()
{
 	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene root"));

	GateFrame = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Gate frame"));
	GateFrame->SetupAttachment(RootComponent);

	GateRightDoor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Gate right door"));
	GateRightDoor->SetupAttachment(GateFrame);

	GateLeftDoor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Gate left door"));
	GateLeftDoor->SetupAttachment(GateFrame);
}

void AGate::GateOpenTimerTick()
{
	float elapsed = GetWorld()->GetTimerManager().GetTimerElapsed(GateOpenTimer);
	TotalElapsed += elapsed;

	float alpha = GateOpenCurve->GetFloatValue(TotalElapsed);

	FRotator rightDoorRotation = FMath::Lerp(RightDoorOriginRotator, RightDoorTargetRotator, alpha);
	FRotator leftDoorRotation = FMath::Lerp(LeftDoorOriginRotator, LeftDoorTargetRotator, alpha);

	GateRightDoor->SetRelativeRotation(rightDoorRotation);
	GateLeftDoor->SetRelativeRotation(leftDoorRotation);

	if (TotalElapsed >= TimeToOpen)
	{
		GetWorld()->GetTimerManager().ClearTimer(GateOpenTimer);
		SetNewViewTarget(OriginViewTarget);
		EnablePlayerInput(true);
	}
}

void AGate::GateCloseTimerTick()
{
	float elapsed = GetWorld()->GetTimerManager().GetTimerElapsed(GateOpenTimer);
	TotalElapsed += elapsed;

	float alpha = GateCloseCurve->GetFloatValue(TotalElapsed);

	FRotator rightDoorRotation = FMath::Lerp(RightDoorOriginRotator, RightDoorTargetRotator, alpha);
	FRotator leftDoorRotation = FMath::Lerp(LeftDoorOriginRotator, LeftDoorTargetRotator, alpha);

	GateRightDoor->SetRelativeRotation(rightDoorRotation);
	GateLeftDoor->SetRelativeRotation(leftDoorRotation);

	if (TotalElapsed >= TimeToClose)
	{
		GetWorld()->GetTimerManager().ClearTimer(GateCloseTimer);
	}
}

void AGate::SetNewViewTarget(AActor* NewViewTarget)
{
	if (!NewViewTarget) return;

	GetWorld()->GetFirstPlayerController()->SetViewTargetWithBlend(NewViewTarget, ViewTargetBlendTime);
}

void AGate::EnablePlayerInput(bool bFlag)
{
	auto controller = GetWorld()->GetFirstPlayerController();
	if (bFlag && !controller->InputEnabled())
	{
		controller->EnableInput(controller);
	}
	else if (!bFlag && controller->InputEnabled())
	{
		controller->DisableInput(controller);
	}
}

void AGate::BeginPlay()
{
	Super::BeginPlay();

	OriginViewTarget = GetWorld()->GetFirstPlayerController()->GetViewTarget();
}

void AGate::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGate::OpenGate()
{
	if (GateOpenTimer.IsValid()) return;

	SetNewViewTarget(ViewTarget);
	EnablePlayerInput(false);

	TotalElapsed = 0;
	RightDoorOriginRotator = GateRightDoor->GetRelativeRotation();
	LeftDoorOriginRotator = GateLeftDoor->GetRelativeRotation();

	RightDoorTargetRotator = FRotator(0, OpenAngle, 0);
	LeftDoorTargetRotator = FRotator(0, -OpenAngle, 0);

	FTimerHandle handle;
	FTimerDelegate delegate;
	delegate.BindLambda([this] { 
		GetWorld()->GetTimerManager().SetTimer(GateOpenTimer, this, &AGate::GateOpenTimerTick, 0.01f, true);
	});

	// Wait to view target blend end before open
	GetWorld()->GetTimerManager().SetTimer(handle, delegate, ViewTargetBlendTime, false);
}

void AGate::CloseGate()
{
	if (GateCloseTimer.IsValid()) return;

	TotalElapsed = 0;
	RightDoorOriginRotator = GateRightDoor->GetRelativeRotation();
	LeftDoorOriginRotator = GateLeftDoor->GetRelativeRotation();

	RightDoorTargetRotator = FRotator(0, 0, 0);
	LeftDoorTargetRotator = FRotator(0, 0, 0);

	GetWorld()->GetTimerManager().SetTimer(GateOpenTimer, this, &AGate::GateCloseTimerTick, 0.01f, true);
}

