// Ulyan Yunakh.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gate.generated.h"

UCLASS()
class SPOOKYSEASON_API AGate : public AActor
{
	GENERATED_BODY()
	
public:	
	AGate();

private:
	UFUNCTION()
	void GateOpenTimerTick();

	UFUNCTION()
	void GateCloseTimerTick();

	UFUNCTION()
	void SetNewViewTarget(AActor* NewViewTarget);

	UFUNCTION()
	void EnablePlayerInput(bool bFlag);

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void OpenGate();

	UFUNCTION(BlueprintCallable)
	void CloseGate();

private:
	UPROPERTY()
	FTimerHandle GateOpenTimer;

	UPROPERTY()
	FTimerHandle GateCloseTimer;

	UPROPERTY()
	float TotalElapsed;

	UPROPERTY()
	FRotator RightDoorOriginRotator;

	UPROPERTY()
	FRotator RightDoorTargetRotator;

	UPROPERTY()
	FRotator LeftDoorOriginRotator;

	UPROPERTY()
	FRotator LeftDoorTargetRotator;

	UPROPERTY()
	AActor* OriginViewTarget;

public:

	// Components

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gate)
	UStaticMeshComponent* GateFrame;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gate)
	UStaticMeshComponent* GateRightDoor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gate)
	UStaticMeshComponent* GateLeftDoor;

	// Defaults

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gate)
	float TimeToOpen = 3.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gate)
	float TimeToClose = 3.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gate)
	float OpenAngle = 90.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gate)
	float ViewTargetBlendTime = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gate)
	UCurveFloat* GateOpenCurve;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gate)
	UCurveFloat* GateCloseCurve;

	// In scene defaults

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gate)
	ACameraActor* ViewTarget;
};
