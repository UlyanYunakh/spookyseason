// Ulyan Yunakh.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GateActivator.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SPOOKYSEASON_API UGateActivator : public UActorComponent
{
	GENERATED_BODY()

public:	
	UGateActivator();

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable)
	void OpenGate();

	UFUNCTION(BlueprintCallable)
	void CloseGate();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gate)
	class AGate* Gate;
		
};
