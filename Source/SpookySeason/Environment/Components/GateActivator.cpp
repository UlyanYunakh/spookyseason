// Ulyan Yunakh.


#include "GateActivator.h"
#include <SpookySeason/Environment/Gate.h>

UGateActivator::UGateActivator()
{
	PrimaryComponentTick.bCanEverTick = false;
}


void UGateActivator::BeginPlay()
{
	Super::BeginPlay();
	
	if (!Gate)
	{
		DestroyComponent();
	}
}

void UGateActivator::OpenGate()
{
	if (Gate)
	{
		Gate->OpenGate();
	}
}

void UGateActivator::CloseGate()
{
	if (Gate)
	{
		Gate->CloseGate();
	}
}

