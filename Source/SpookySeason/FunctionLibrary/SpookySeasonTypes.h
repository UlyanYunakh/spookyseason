// Ulyan Yunakh.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SpookySeasonTypes.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Walt UMETA(DisplayName = "Walt"),
	Sprint UMETA(DisplayName = "Sprint"),
};

USTRUCT(BlueprintType)
struct FMovementSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float Walk = 400.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float Sprint = 600.0f;

	float GetSpeedForState(EMovementState State)
	{
		switch (State)
		{
		case EMovementState::Sprint:
			return Sprint;
		case EMovementState::Walt:
		default:
			return Walk;
		}
	}

};

USTRUCT(BlueprintType)
struct FTarget
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Targeting)
	FVector Origin = FVector::Zero();
};

UCLASS()
class SPOOKYSEASON_API USpookySeasonTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
