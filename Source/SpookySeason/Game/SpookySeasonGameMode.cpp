// Copyright Epic Games, Inc. All Rights Reserved.

#include "SpookySeasonGameMode.h"
#include "SpookySeasonPlayerController.h"
#include "../Character/SpookySeasonCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASpookySeasonGameMode::ASpookySeasonGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASpookySeasonPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprints/Character/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}