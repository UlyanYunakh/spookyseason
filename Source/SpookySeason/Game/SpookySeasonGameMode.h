// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SpookySeasonGameMode.generated.h"

UCLASS(minimalapi)
class ASpookySeasonGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASpookySeasonGameMode();
};



